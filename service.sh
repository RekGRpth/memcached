#!/bin/sh -ex

#docker build --tag rekgrpth/memcached . || exit $?
#docker push rekgrpth/memcached || exit $?
docker pull rekgrpth/memcached || exit $?
docker network create --attachable --driver overlay docker || echo $?
docker service rm memcached || echo $?
docker service create \
    --env LANG=ru_RU.UTF-8 \
    --env TZ=Asia/Yekaterinburg \
    --hostname tasks.memcached \
    --name memcached \
    --network name=docker \
    rekgrpth/memcached
