FROM alpine
RUN set -ex \
    && apk add --no-cache --virtual .memcached-rundeps \
        memcached \
        su-exec \
        tzdata \
    && echo done
CMD [ "memcached" ]
ENTRYPOINT [ "su-exec", "memcached" ]
