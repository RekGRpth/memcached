#!/bin/sh -ex

#docker build --tag rekgrpth/memcached . || exit $?
#docker push rekgrpth/memcached || exit $?
docker pull rekgrpth/memcached || exit $?
docker network create --attachable --opt com.docker.network.bridge.name=docker docker || echo $?
docker stop memcached || echo $?
docker rm memcached || echo $?
docker run \
    --detach \
    --env LANG=ru_RU.UTF-8 \
    --env TZ=Asia/Yekaterinburg \
    --hostname memcached \
    --name memcached \
    --network name=docker \
    --restart always \
    rekgrpth/memcached
